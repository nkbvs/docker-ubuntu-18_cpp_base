FROM ubuntu:18.04

LABEL maintainer="Denis Tulyakov <dtulyakov@gmail.com>" \
  org.label-schema.build-date=$BUILD_DATE \
  org.label-schema.docker.cmd="docker run --rm -it -v ${PWD}:/app nkbvs/ubuntu-18_cpp_base bash" \
  org.label-schema.description="Ubuntu C++ builder" \
  org.label-schema.name="ubuntu-18_cpp_base" \
  org.label-schema.schema-version="18.04" \
  org.label-schema.url="https://ubuntu.org" \
  org.label-schema.vcs-ref=$VCS_REF \
  org.label-schema.vcs-url="https://bitbucket.org/nkbvs/docker-ubuntu-18_cpp_base" \
  org.label-schema.vendor="dtulyakov" \
  org.label-schema.version=$VERSION

ENV DEBIAN_FRONTEND="noninteractive"

USER root

RUN set -x \
  && apt-get update -qq \
  && apt-get install -qy \
     autoconf \
     automake \
     bash-completion \
     build-essential \
     bzip2 \
     ca-certificates \
     clang \
     clang-tidy \
     cmake \
     cmake-curses-gui \
     curl \
     debhelper \
     dh-systemd \
     fakeroot \
     gdbserver \
#     git \
     git-buildpackage \
     gnupg2 \
     libasio-dev \
     libcap-dev \
     libeigen3-dev \
     libgdal-dev \
     libgeographic-dev \
     libgles2-mesa-dev \
     libglib2.0-dev \
     libgtest-dev \
     libjemalloc-dev \
     libjson-c-dev \
     liblog4cxx-dev \
     liblz4-dev \
     libopencv-dev \
     libopenscenegraph-3.4-dev \
     libpcre3-dev \
     libsystemd-dev \
     libtinyxml2-dev \
     libtinyxml2.6.2v5 \
     libtool \
     libudev-dev \
     libusb-1.0-0-dev \
     libyaml-cpp-dev \
     libzzip-dev \
#     linux-headers-$(uname -r) \
     llvm \
     llvm-dev \
     lsb-release \
     net-tools \
     openssl \
     pkg-config \
     python-dev \
     python3-pip \
     python3-setuptools \
     qt5-default \
     qtdeclarative5-dev \
     rapidjson-dev \
     software-properties-common \
     unzip \
     usbutils \
     valgrind \
     vim \
     wget \
     zlib1g-dev \
  && apt-get autoremove -y \
  && rm -fr /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/apt/archives/*

# vim: set filetype=dockerfile et sw=2 ts=2:
